package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 这里是类说明.
 *
 * @className: EurekaMain7002
 * @author: hyc
 * @date: 2022-01-12 11:18
 */
// 开启Eureka的支持
@EnableEurekaServer
@SpringBootApplication
public class EurekaMain7002 {
    
    public static void main(String[] args) {
        
        SpringApplication.run(EurekaMain7002.class, args);
    }
}