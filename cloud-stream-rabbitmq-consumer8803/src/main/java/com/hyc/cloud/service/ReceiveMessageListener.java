package com.hyc.cloud.service;

import org.springframework.messaging.Message;

/**
 * 这里是类说明.
 *
 * @className: ReceiveMessageListener
 * @author: hyc
 * @date: 2022-01-25 19:11
 */
public interface ReceiveMessageListener {

    void receiveMessage(Message<String> message);
}
