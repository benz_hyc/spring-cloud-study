package com.hyc.cloud.service.impl;

import com.hyc.cloud.service.PaymentHystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Component;

/**
 * 这里是类说明.
 *
 * @className: PaymentHystrixServiceImpl
 * @author: hyc
 * @date: 2022-01-17 15:16
 */
@Component  // 此注解必须加 不加的话会出现异常
public class PaymentHystrixServiceImpl implements PaymentHystrixService {

    @Override
    public String paymentInfo_OK(Integer id) {
        return "服务调用失败：PaymentFallbackService fall back paymentInfo_OK o(╥﹏╥)o";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "服务调用失败：PaymentFallbackService fall back paymentInfo_TimeOut o(╥﹏╥)o";
    }

    @HystrixCommand(commandProperties = {@HystrixProperty(name = "circuitBreaker.enabled", value = "true"),// 是否开启熔断器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60")}) // 失败率达到多少进行熔断
    @Override
    public String paymentCircuitBreaker(Integer id) {
        return "服务调用异常 服务熔断 PaymentFallbackService fall back paymentCircuitBreaker o(╥﹏╥)o";
    }
}