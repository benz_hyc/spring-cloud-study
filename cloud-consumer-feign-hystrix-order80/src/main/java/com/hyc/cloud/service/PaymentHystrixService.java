package com.hyc.cloud.service;

import com.hyc.cloud.service.impl.PaymentHystrixServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 这里是类说明.
 *
 * @className: PaymentHystrixService
 * @author: hyc
 * @date: 2022-01-17 11:03
 */
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT", fallback = PaymentHystrixServiceImpl.class)
@Service
public interface PaymentHystrixService {

    /**
     * 正常访问，一切OK
     * @title paymentInfo_OK
     * @author hyc
     * @date 2022/1/17 11:05
     * @param id    id
     * @return java.lang.String
     */
    @GetMapping("/payment/hystrix/ok/{id}")
    String paymentInfo_OK(@PathVariable("id") Integer id);

    /**
     * 超时访问，演示降级
     * @title paymentInfo_TimeOut
     * @author hyc
     * @date 2022/1/17 11:05
     * @param id    id
     * @return java.lang.String
     */
    @GetMapping("/payment/hystrix/timeout/{id}")
    String paymentInfo_TimeOut(@PathVariable("id") Integer id);

    /**
     * 服务熔断测试方法
     * @title paymentCircuitBreaker
     * @author hyc
     * @date 2022/1/17 16:04
     * @param id    id
     * @return java.lang.String
     */
    @GetMapping("/payment/circuit/{id}")
    String paymentCircuitBreaker(@PathVariable("id") Integer id);
}