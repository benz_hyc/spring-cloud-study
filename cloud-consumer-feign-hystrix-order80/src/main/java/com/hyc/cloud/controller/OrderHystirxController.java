package com.hyc.cloud.controller;

import com.hyc.cloud.service.PaymentHystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 这里是类说明.
 *
 * @className: OrderHystirxController
 * @author: hyc
 * @date: 2022-01-17 11:05
 */
@RestController
@Slf4j
@DefaultProperties(defaultFallback = "globalFallbackMethod")
public class OrderHystirxController {

    @Resource
    private PaymentHystrixService paymentHystrixService;

    /**
     * 正常访问，一切OK
     * @title paymentInfo_OK
     * @author hyc
     * @date 2022/1/17 11:06
     * @param id    id
     * @return java.lang.String
     */
    @GetMapping("/consumer/payment/hystrix/ok/{id}")
    public String paymentInfo_OK(@PathVariable("id") Integer id) {
        String result = paymentHystrixService.paymentInfo_OK(id);
        return result;
    }

    /**
     * 超时访问，演示降级
     * @title paymentInfo_TimeOut
     * @author hyc
     * @date 2022/1/17 11:06
     * @param id    id
     * @return java.lang.String
     */
    // 这里没有指定fallbackMethod方法，配合@DefaultProperties(defaultFallback = "globalFallback")使用服务降级
    @HystrixCommand
    // 此处的优先级大于全局服务降级方法
//    @HystrixCommand(fallbackMethod = "paymentInfo_TimeoutFallback"
//            , commandProperties = {@HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="1500")})
    @GetMapping("/consumer/payment/hystrix/timeout/{id}")
    public String paymentInfo_TimeOut(@PathVariable("id") Integer id) {
//        int i = 10 / 0;
        String result = paymentHystrixService.paymentInfo_TimeOut(id);
        return result;
    }

    /**
     * paymentInfo_TimeOut的降级方法
     * @title paymentInfo_TimeoutFallback
     * @author hyc
     * @date 2022/1/17 14:25
     * @param id    id
     * @return java.lang.String
     */
    public String paymentInfo_TimeoutFallback(Integer id) {
        return "我是消费者80,对方支付系统繁忙请10秒钟后再试或者自己运行出错请检查自己,o(╥﹏╥)o";
    }

    /**
     * 全局统一的服务降级方法
     * @title globalFallback
     * @author hyc
     * @date 2022/1/17 14:40
     * @return java.lang.String
     */
    public String globalFallbackMethod() {
        return "Global Fallback 我是消费者80,对方支付系统繁忙请10秒钟后再试或者自己运行出错请检查自己,o(╥﹏╥)o";
    }

    /**
     * 服务熔断测试方法
     * @title paymentCircuitBreaker
     * @author hyc
     * @date 2022/1/17 16:05
     * @param id    id
     * @return java.lang.String
     */
    @GetMapping("/consumer/payment/circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id) {
        String result = paymentHystrixService.paymentCircuitBreaker(id);
        log.info("****result: " + result);
        return result;
    }
}