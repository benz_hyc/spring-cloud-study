package com.hyc.cloud.controller;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.entities.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 这里是类说明.
 *
 * @className: PaymentController
 * @author: hyc
 * @date: 2022-02-15 12:00
 */
@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    /**
     * 负载均衡测试方法
     * @title paymentInfo
     * @author hyc
     * @date 2022/2/15 11:28
     * @param id    id
     * @return com.hyc.cloud.entities.CommonResult<com.hyc.cloud.entities.Payment>
     */
    @GetMapping("/paymentInfo/{id}")
    public CommonResult<Payment> paymentInfo(@PathVariable("id") Long id) {
        if (id < 1) {
            return new CommonResult<>(500, "参数错误");
        }
        return new CommonResult(200, "操作成功", new Payment(id, serverPort + " - paymentInfo"));
    }
}