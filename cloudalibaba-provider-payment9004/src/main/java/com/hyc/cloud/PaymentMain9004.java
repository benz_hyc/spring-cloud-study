package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: PaymentMain9004
 * @author: hyc
 * @date: 2022-02-15 11:59
 */
@EnableDiscoveryClient
@SpringBootApplication
public class PaymentMain9004 {

    public static void main(String[] args) {

        SpringApplication.run(PaymentMain9004.class, args);
    }
}