package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * 这里是类说明.
 *
 * @className: HystrixDashboardMain9001
 * @author: hyc
 * @date: 2022-01-17 17:22
 */
// 开启Hystrix服务监控的支持
@EnableHystrixDashboard
@SpringBootApplication
public class HystrixDashboardMain9001 {
    
    public static void main(String[] args) {
        
        SpringApplication.run(HystrixDashboardMain9001.class, args);
    }
}