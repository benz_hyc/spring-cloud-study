package com.hyc.cloud.controller;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.service.AccountService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 这里是类说明.
 *
 * @className: AccountController
 * @author: hyc
 * @date: 2022-02-17 20:54
 */
@RestController
public class AccountController {

    @Resource
    private AccountService accountService;

    /**
     * 扣减账户余额
     * @title decrease
     * @author hyc
     * @date 2022年02月17日 20:54
     * @param userId    用户id
     * @param money 金额
     * @return CommonResult
     */
    @RequestMapping("/account/decrease")
    public CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money){
        accountService.decrease(userId,money);
        return new CommonResult(200,"扣减账户余额成功！");
    }

}