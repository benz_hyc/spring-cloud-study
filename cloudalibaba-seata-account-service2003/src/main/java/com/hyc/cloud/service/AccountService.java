package com.hyc.cloud.service;

import java.math.BigDecimal;

/**
 * 这里是类说明.
 *
 * @className: AccountService
 * @author: hyc
 * @date: 2022-02-17 20:55
 */
public interface AccountService {

    /**
     * 扣减账户余额
     * @title decrease
     * @author hyc
     * @date 2022年02月17日 20:55
     * @param userId    用户id
     * @param money     金额
     * @return void
     */
    void decrease(Long userId, BigDecimal money);
}