package com.hyc.cloud.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * 这里是类说明.
 *
 * @className: AccountDao
 * @author: hyc
 * @date: 2022-02-17 20:57
 */
@Mapper
public interface AccountDao {

    /**
     * 扣减账户余额
     * @title decrease
     * @author hyc
     * @date 2022年02月17日 20:58
     * @param userId    用户id
     * @param money 金额
     * @return int
     */
    int decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);
}