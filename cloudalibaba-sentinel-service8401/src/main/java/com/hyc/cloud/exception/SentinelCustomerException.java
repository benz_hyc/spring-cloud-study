package com.hyc.cloud.exception;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.hyc.cloud.entities.CommonResult;

/**
 * Sentinel自定义限流异常处理类.
 *
 * @className: SentinelCustomerException
 * @author: hyc
 * @date: 2022-02-15 10:55
 */
public class SentinelCustomerException {

    /**
     * 自定义降级限流处理方法 方法必须设置为static的
     * @title exceptionHandler
     * @author hyc
     * @date 2022/2/15 10:56
     * @param exception e
     * @return com.hyc.cloud.entities.CommonResult
     */
    public static CommonResult exceptionHandler(BlockException exception) {
        return new CommonResult(500, "接口异常");
    }
}