package com.hyc.cloud.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.hyc.cloud.service.FlowLimitService;
import org.springframework.stereotype.Service;

/**
 * 这里是类说明.
 *
 * @className: FlowLimitServiceImpl
 * @author: hyc
 * @date: 2022-02-14 10:46
 */
@Service
public class FlowLimitServiceImpl implements FlowLimitService {

    // 将此方法标注为sentinel的资源。value=资源名
    @SentinelResource("chainModeTest")
    @Override
    public String chainModeTest() {
        System.out.println("**** chain mode test ****");
        return "**** chain mode test ****";
    }
}