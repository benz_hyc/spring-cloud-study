package com.hyc.cloud.service;

/**
 * 这里是类说明.
 *
 * @className: FlowLimitService
 * @author: hyc
 * @date: 2022-02-14 10:46
 */
public interface FlowLimitService {

    /**
     * 链路流控模式调用测试
     * @title chainModeTest
     * @author hyc
     * @date 2022/2/14 10:49
     * @param
     * @return java.lang.String
     */
    String chainModeTest();
}
