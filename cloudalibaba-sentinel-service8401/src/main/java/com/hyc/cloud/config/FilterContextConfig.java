package com.hyc.cloud.config;

import com.alibaba.csp.sentinel.adapter.servlet.CommonFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

/**
 * 这里是类说明.
 *
 * @className: FilterContextConfig
 * @author: hyc
 * @date: 2022-02-14 14:40
 */
@Configuration
public class FilterContextConfig {

    /**
     * SCA -> Spring Cloud Alibaba
     * 从1.6.3 版本开始，Sentinel Web filter默认收敛所有URL的入口context，因此链路限流不生效。
     * 1.7.0 版本开始（对应SCA的2.1.1.RELEASE)，官方在CommonFilter 引入了
     * WEB_CONTEXT_UNIFY 参数，用于控制是否收敛context。将其配置为 false 即可根据不同的URL 进行链路限流。
     * SCA 2.1.1.RELEASE之后的版本,可以通过配置spring.cloud.sentinel.web-context-unify=false即可关闭收敛
     * 我们当前使用的版本是SpringCloud Alibaba 2.1.0.RELEASE，无法实现链路限流。
     * 我们使用2.1.1.RELEASE，需要写代码的形式实现
     * @title sentinelFilter
     * @author hyc
     * @date 2022/2/14 15:22
     * @param
     * @return org.springframework.boot.web.servlet.FilterRegistrationBean
     */
    @Bean
    public FilterRegistrationBean sentinelFilter() {

        FilterRegistrationBean<Filter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new CommonFilter());
        bean.addUrlPatterns("/*");

        // 禁止收敛URL的入口context
        // 关闭url PATH路径聚合 Sentinel默认会将Controller方法做context聚合，导致链路模式的流控失效
        bean.addInitParameter(CommonFilter.WEB_CONTEXT_UNIFY, "false");
        bean.setName("sentinelFilter");
        bean.setOrder(1);

        return bean;
    }
}