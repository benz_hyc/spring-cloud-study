package com.hyc.cloud.component;

import com.alibaba.csp.sentinel.adapter.servlet.callback.RequestOriginParser;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Sentinel是通过RequestOriginParser这个接口的parseOrigin来获取请求的来源的.
 *
 * @className: CustomerOriginParser
 * @author: hyc
 * @date: 2022-02-15 15:55
 */
@Component
public class CustomerOriginParser implements RequestOriginParser {

    @Override
    public String parseOrigin(HttpServletRequest request) {

        return request.getRemoteAddr();
    }
}