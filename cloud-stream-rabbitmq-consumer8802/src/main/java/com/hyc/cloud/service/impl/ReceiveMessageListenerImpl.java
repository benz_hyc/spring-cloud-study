package com.hyc.cloud.service.impl;

import com.hyc.cloud.service.ReceiveMessageListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

/**
 * 这里是类说明.
 *
 * @className: ReceiveMessageListenerImpl
 * @author: hyc
 * @date: 2022-01-25 17:17
 */
@EnableBinding(Sink.class)
@Service
public class ReceiveMessageListenerImpl implements ReceiveMessageListener {

    @Value("${server.port}")
    private String serverPort;

    @StreamListener(Sink.INPUT)
    @Override
    public void receiveMessage(Message<String> message) {
        System.out.println("消费者1号，------->接收到的消息：" + message.getPayload()+"\t port: "+serverPort);
    }
}