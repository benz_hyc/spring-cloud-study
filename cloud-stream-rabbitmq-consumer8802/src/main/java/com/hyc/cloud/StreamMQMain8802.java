package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: StreamMQMain8802
 * @author: hyc
 * @date: 2022-01-25 17:15
 */
@EnableDiscoveryClient
@SpringBootApplication
public class StreamMQMain8802 {
    
    public static void main(String[] args) {
        
        SpringApplication.run(StreamMQMain8802.class, args);
    }
}