package com.hyc.cloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * 这里是类说明.
 *
 * @className: MyBatisConfig
 * @author: hyc
 * @date: 2022-02-17 20:33
 */
@Configuration
@MapperScan({"com.hyc.cloud.dao"})
public class MyBatisConfig {
}