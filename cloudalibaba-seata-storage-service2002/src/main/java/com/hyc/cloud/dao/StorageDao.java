package com.hyc.cloud.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 这里是类说明.
 *
 * @className: StorageDao
 * @author: hyc
 * @date: 2022-02-17 20:35
 */
@Mapper
public interface StorageDao {

    /**
     * 扣减库存
     * @title decrease
     * @author hyc
     * @date 2022年02月17日 20:35
     * @param productId 产品id
     * @param count 数量
     * @return void
     */
    int decrease(@Param("productId") Long productId, @Param("count") Integer count);
}