package com.hyc.cloud.controller;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.service.StorageService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 这里是类说明.
 *
 * @className: StorageController
 * @author: hyc
 * @date: 2022-02-17 20:40
 */
@RestController
public class StorageController {

    @Resource
    private StorageService storageService;

    /**
     * 扣减库存
     * @title decrease
     * @author hyc
     * @date 2022年02月17日 20:41
     * @param productId 产品id
     * @param count 数量
     * @return com.hyc.cloud.entities.CommonResult
     */
    @RequestMapping("/storage/decrease")
    public CommonResult decrease(Long productId, Integer count) {
        storageService.decrease(productId, count);
        return new CommonResult(200,"扣减库存成功！");
    }

}