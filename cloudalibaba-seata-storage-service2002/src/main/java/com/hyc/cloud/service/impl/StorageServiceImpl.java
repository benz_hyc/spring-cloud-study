package com.hyc.cloud.service.impl;

import com.hyc.cloud.dao.StorageDao;
import com.hyc.cloud.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 这里是类说明.
 *
 * @className: StorageServiceImpl
 * @author: hyc
 * @date: 2022-02-17 20:39
 */
@Service
@Slf4j
public class StorageServiceImpl implements StorageService {

    @Resource
    private StorageDao storageDao;

    @Override
    public void decrease(Long productId, Integer count) {
        log.info("------->storage-service中扣减库存开始");
        storageDao.decrease(productId,count);
        log.info("------->storage-service中扣减库存结束");
    }
}