package com.hyc.cloud.service;

/**
 * 这里是类说明.
 *
 * @className: StorageServiceImpl
 * @author: hyc
 * @date: 2022-02-17 20:38
 */
public interface StorageService {

    /**
     * 扣减库存
     * @title decrease
     * @author hyc
     * @date 2022年02月17日 20:38
     * @param productId 产品id
     * @param count 产品数量
     * @return void
     */
    void decrease(Long productId, Integer count);
}