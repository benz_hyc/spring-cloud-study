package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 这里是类说明.
 *
 * @className: SeataStorageServiceApplication2002
 * @author: hyc
 * @date: 2022-02-17 20:31
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SeataStorageServiceApplication2002 {

    public static void main(String[] args) {

        SpringApplication.run(SeataStorageServiceApplication2002.class, args);
    }

}