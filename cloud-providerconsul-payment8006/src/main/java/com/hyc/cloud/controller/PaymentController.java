package com.hyc.cloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * 这里是类说明.
 *
 * @className: PaymentController
 * @author: hyc
 * @date: 2022-01-13 18:54
 */
@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping("/payment/consul")
    public String paymentInfo() {
        return "SpringCloud with consul: " + serverPort + "\t\t" + UUID.randomUUID().toString();
    }

}