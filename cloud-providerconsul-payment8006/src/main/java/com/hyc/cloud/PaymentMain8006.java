package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: PaymentMain8006
 * @author: hyc
 * @date: 2022-01-13 18:51
 */
@EnableDiscoveryClient
@SpringBootApplication
public class PaymentMain8006 {
    
    public static void main(String[] args) {
        
        SpringApplication.run(PaymentMain8006.class, args);
    }
}