package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: PaymentMain8001
 * @author: hyc
 * @date: 2022-01-11 17:41
 */
// 开启eureka的支持
@EnableDiscoveryClient
@SpringBootApplication
public class PaymentMain8001 {

    public static void main(String[] args) {

        SpringApplication.run(PaymentMain8001.class, args);
    }
}