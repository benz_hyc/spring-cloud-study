package com.hyc.cloud.controller;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.entities.Payment;
import com.hyc.cloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 这里是类说明.
 *
 * @className: PaymentController
 * @author: hyc
 * @date: 2022-01-11 17:49
 */
@RestController
@Slf4j
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private PaymentService paymentService;

    @PostMapping(value = "/payment/create")
    public CommonResult create(@RequestBody Payment payment) {
        int result = paymentService.create(payment);
        log.info("*****新增结果:" + result);

        if (result > 0) {
            return new CommonResult(200, "插入成功,返回结果" + result + "\t 服务端口：" + serverPort, payment);
        } else {
            return new CommonResult(444, "插入失败", null);
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        log.info("*****查询结果:{}", payment);
        if (payment != null) {
            return new CommonResult(200, "查询成功" + "\t 服务端口：" + serverPort, payment);
        } else {
            return new CommonResult(444, "没有对应记录,查询ID: " + id, null);
        }
    }

    /**
     * 手写轮询负载均衡算法测试方法
     * @title paymentLb
     * @author hyc
     * @date 2022/1/14 14:36
     * @param
     * @return java.lang.String
     */
    @GetMapping("/payment/lb")
    public String paymentLb(){
        return serverPort;
    }

    /**
     * 测试OpenFeign超时
     * @title paymentFeignTimeout
     * @author hyc
     * @date 2022/1/14 16:30
     * @param
     * @return java.lang.String
     */
    @GetMapping("/payment/feign/timeout")
    public String paymentFeignTimeout() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return serverPort;
    }

    /**
     * zuul负载均衡测试方法
     * @title zuulLoadBalancer
     * @author hyc
     * @date 2022年01月17日 22:08
     * @param
     * @return java.lang.String
     */
    @GetMapping("/payment/zuulLoadBalancer")
    public String zuulLoadBalancer() {
        return "payment " + serverPort;
    }

    /**
     * 链路追踪zipkin测试
     * @title zipkin
     * @author hyc
     * @date 2022年02月09日 23:51
     * @param
     * @return java.lang.String
     */
    @GetMapping("/payment/zipkin")
    public String zipkin() {
        return serverPort + " -- zipkin";
    }
}