package com.hyc.cloud.service;

import com.hyc.cloud.entities.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * 这里是类说明.
 *
 * @className: PaymentService
 * @author: hyc
 * @date: 2022-01-11 17:48
 */
public interface PaymentService {

    int create(Payment payment);

    Payment getPaymentById(@Param("id") Long id);

}
