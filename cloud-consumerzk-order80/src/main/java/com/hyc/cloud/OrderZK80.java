package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: OrderZK80
 * @author: hyc
 * @date: 2022-01-13 15:48
 */
@EnableDiscoveryClient
@SpringBootApplication
public class OrderZK80 {

    public static void main(String[] args) {

        SpringApplication.run(OrderZK80.class, args);
    }
}