package com.hyc.cloud.controller;

import com.hyc.cloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 这里是类说明.
 *
 * @className: PaymentController
 * @author: hyc
 * @date: 2022-01-17 10:28
 */
@RestController
@Slf4j
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;


    /**
     * 使用Gateway中filter的  - AddRequestHeader=requestId, 123 时候 必须使用Post请求方式
     * @title paymentInfo_OK
     * @author hyc
     * @date 2022/1/19 15:48
     * @param request   r
     * @param id    id
     * @param username 用户名
     * @return java.lang.String
     */
    @PostMapping("/payment/hystrix/ok/{id}")
    public String paymentInfo_OK(HttpServletRequest request, @PathVariable("id") Integer id, String username) {
        String result = paymentService.paymentInfo_OK(id);
        log.info("****result: " + result);

        String requestId = request.getHeader("requestId");

        return result;
    }

    @GetMapping("/payment/hystrix/timeout/{id}")
    public String paymentInfo_TimeOut(@PathVariable("id") Integer id) throws InterruptedException {
        String result = paymentService.paymentInfo_TimeOut(id);
        log.info("****result: " + result);
        return result;
    }

    /**
     * 服务熔断测试方法
     * @title paymentCircuitBreaker
     * @author hyc
     * @date 2022/1/17 16:03
     * @param id    id
     * @return java.lang.String
     */
    @GetMapping("/payment/circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id) {
        String result = paymentService.paymentCircuitBreaker(id);
        log.info("****result: " + result);
        return result;
    }

    /**
     * Gateway自定义filter侧首方法
     * @title getServerPort
     * @author hyc
     * @date 2022/1/20 11:13
     * @param name  name
     * @return java.lang.String
     */
    @RequestMapping("/payment/filter/getServerPort")
    public String getServerPort(String name) {
        return name + " -- " + serverPort;
    }
}