package com.hyc.cloud.service;

/**
 * 这里是类说明.
 *
 * @className: PaymentService
 * @author: hyc
 * @date: 2022-01-17 10:22
 */
public interface PaymentService {

    /**
     * 正常访问，一切OK
     * @title paymentInfo_OK
     * @author hyc
     * @date 2022/1/17 10:22
     * @param id    id
     * @return java.lang.String
     */
    String paymentInfo_OK(Integer id);

    /**
     * 超时访问，演示降级
     * @title paymentInfo_TimeOut
     * @author hyc
     * @date 2022/1/17 10:23
     * @param id    id
     * @return java.lang.String
     */
    String paymentInfo_TimeOut(Integer id);

    /**
     * 服务熔断测试方法
     * @title paymentCircuitBreaker
     * @author hyc
     * @date 2022/1/17 16:01
     * @param id    id
     * @return java.lang.String
     */
    String paymentCircuitBreaker(Integer id);

}
