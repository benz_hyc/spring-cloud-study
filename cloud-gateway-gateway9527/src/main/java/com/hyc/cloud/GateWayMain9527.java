package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: GateWayMain9527
 * @author: hyc
 * @date: 2022-01-18 16:23
 */
@EnableDiscoveryClient
@SpringBootApplication
public class GateWayMain9527 {

    public static void main(String[] args) {

        SpringApplication.run(GateWayMain9527.class, args);
    }
}