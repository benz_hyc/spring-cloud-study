package com.hyc.cloud.service.impl;

import com.hyc.cloud.dao.PaymentDao;
import com.hyc.cloud.entities.Payment;
import com.hyc.cloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 这里是类说明.
 *
 * @className: PaymentServiceImpl
 * @author: hyc
 * @date: 2022-01-11 17:48
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;


    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }

}