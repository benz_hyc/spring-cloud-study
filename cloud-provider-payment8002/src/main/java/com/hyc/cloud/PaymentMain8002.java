package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: PaymentMain8002
 * @author: hyc
 * @date: 2022-01-12 17:19
 */
// 开启eureka的支持
@EnableDiscoveryClient
@SpringBootApplication
public class PaymentMain8002 {

    public static void main(String[] args) {

        SpringApplication.run(PaymentMain8002.class, args);
    }
}