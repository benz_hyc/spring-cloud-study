package com.hyc.cloud.dao;

import com.hyc.cloud.entities.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 这里是类说明.
 *
 * @className: PaymentDao
 * @author: hyc
 * @date: 2022-01-11 17:46
 */
@Mapper
public interface PaymentDao {

    int create(Payment payment);

    Payment getPaymentById(@Param("id") Long id);

}
