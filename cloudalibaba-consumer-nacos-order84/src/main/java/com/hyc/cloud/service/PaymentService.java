package com.hyc.cloud.service;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.entities.Payment;
import com.hyc.cloud.service.impl.PaymentServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 使用 fallback 方式是无法获取异常信息的，
 * 如果想要获取异常信息，可以使用 fallbackFactory参数
 * @className: PaymentService
 * @author: hyc
 * @date: 2022-02-15 14:25
 */
@FeignClient(value = "nacos-payment-provider", fallback = PaymentServiceImpl.class)
public interface PaymentService {

    @GetMapping("/paymentInfo/{id}")
    CommonResult<Payment> paymentInfo(@PathVariable("id") Long id);
}