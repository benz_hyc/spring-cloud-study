package com.hyc.cloud.service.impl;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.entities.Payment;
import com.hyc.cloud.service.PaymentService;
import org.springframework.stereotype.Service;

/**
 * 这里是类说明.
 *
 * @className: PaymentServiceImpl
 * @author: hyc
 * @date: 2022-02-15 14:27
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Override
    public CommonResult<Payment> paymentInfo(Long id) {
        return new CommonResult<>(444,"服务降级返回,没有该流水信息",new Payment(id, "errorSerial......"));
    }
}