package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 这里是类说明.
 *
 * @className: OrderNacosMain84
 * @author: hyc
 * @date: 2022-02-15 12:07
 */
// 开启feign的支持
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class OrderNacosMain84 {

    public static void main(String[] args) {

        SpringApplication.run(OrderNacosMain84.class, args);
    }
}