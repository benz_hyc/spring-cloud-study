package com.hyc.cloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 这里是类说明.
 *
 * @className: Payment
 * @author: hyc
 * @date: 2022-01-12 10:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment {

    private Long id;

    private String serial;

}