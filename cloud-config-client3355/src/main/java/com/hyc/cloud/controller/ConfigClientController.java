package com.hyc.cloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 这里是类说明.
 *
 * @className: ConfigClientController
 * @author: hyc
 * @date: 2022-01-20 16:31
 */
// SpringCloudConfig动态刷新注解
@RefreshScope
@RestController
public class ConfigClientController {

    @Value("${config.info}")
    private String configInfo;

    /**
     * SpringCloudConfig配置中心client测试
     * @title getConfigInfo
     * @author hyc
     * @date 2022/1/20 16:32
     * @param
     * @return java.lang.String
     */
    @GetMapping("/getConfigInfo")
    public String getConfigInfo() {
        return configInfo;
    }
}