package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: ConfigClientMain3355
 * @author: hyc
 * @date: 2022-01-20 16:29
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigClientMain3355 {
    
    public static void main(String[] args) {
        
        SpringApplication.run(ConfigClientMain3355.class, args);
    }
}