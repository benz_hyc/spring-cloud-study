package com.hyc.cloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 这里是类说明.
 *
 * @className: PaymentController
 * @author: hyc
 * @date: 2022-02-10 14:18
 */
@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    /**
     * nacos测试方法
     * @title getPayment
     * @author hyc
     * @date 2022/2/10 14:22
     * @param id    id
     * @return java.lang.String
     */
    @GetMapping("/payment/nacos/{id}")
    public String getPayment(@PathVariable("id") Integer id) {
        return "nacos registry, serverPort: " + serverPort + "\t id " + id;
    }
}