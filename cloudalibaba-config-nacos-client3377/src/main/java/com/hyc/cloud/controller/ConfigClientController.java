package com.hyc.cloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 这里是类说明.
 *
 * @className: ConfigClientController
 * @author: hyc
 * @date: 2022-02-10 18:09
 */
@RefreshScope //在控制器类加入@RefreshScope注解使当前类下的配置支持Nacos的动态刷新功能。
@RestController
public class ConfigClientController {

    @Value("${config.info}")
    private String configInfo;

    /**
     * 动态配置刷洗测试方法
     * @title getConfigInfo
     * @author hyc
     * @date 2022/2/10 18:10
     * @param
     * @return java.lang.String
     */
    @GetMapping("/config/info")
    public String getConfigInfo() {
        return configInfo;
    }

}