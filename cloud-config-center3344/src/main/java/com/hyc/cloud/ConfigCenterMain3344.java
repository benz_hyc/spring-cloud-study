package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 这里是类说明.
 *
 * @className: ConfigCenterMain3344
 * @author: hyc
 * @date: 2022-01-20 14:22
 */
// 开启Config的支持
@EnableConfigServer
@SpringBootApplication
public class ConfigCenterMain3344 {

    public static void main(String[] args) {

        SpringApplication.run(ConfigCenterMain3344.class, args);
    }
}