package com.hyc.cloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 这里是类说明.
 *
 * @className: ApplicationContextBean
 * @author: hyc
 * @date: 2022-02-10 15:06
 */
@Configuration
public class ApplicationContextBean {

    @Bean
    @LoadBalanced // 这个注解很重要 可以实现负载均衡
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}