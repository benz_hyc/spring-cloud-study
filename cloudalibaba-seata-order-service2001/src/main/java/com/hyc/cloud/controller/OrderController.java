package com.hyc.cloud.controller;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.entities.Order;
import com.hyc.cloud.service.OrderService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 这里是类说明.
 *
 * @className: OrderController
 * @author: hyc
 * @date: 2022-02-16 15:19
 */
@RestController
public class OrderController {

    @Resource
    private OrderService orderService;

    /**
     * 创建订单
     * @title create
     * @author hyc
     * @date 2022/2/16 15:21
     * @param order o
     * @return com.hyc.cloud.entities.CommonResult
     */
    @PostMapping("/order/create")
    public CommonResult create(Order order) {
        orderService.createOrder(order);
        return new CommonResult(200, "订单创建成功");
    }
}