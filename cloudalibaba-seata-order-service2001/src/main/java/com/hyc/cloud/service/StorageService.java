package com.hyc.cloud.service;

import com.hyc.cloud.entities.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 这里是类说明.
 *
 * @className: StorageService
 * @author: hyc
 * @date: 2022-02-16 15:23
 */
@FeignClient(value = "seata-storage-service")
public interface StorageService {

    /**
     * 扣减库存
     * @title decrease
     * @author hyc
     * @date 2022/2/16 15:24
     * @param productId 产品id
     * @param count 数量
     * @return com.hyc.cloud.entities.CommonResult
     */
    @PostMapping(value = "/storage/decrease")
    CommonResult decrease(@RequestParam("productId") Long productId, @RequestParam("count") Integer count);

}
