package com.hyc.cloud.service;

import com.hyc.cloud.entities.Order;

/**
 * 这里是类说明.
 *
 * @className: OrderService
 * @author: hyc
 * @date: 2022-02-16 15:11
 */
public interface OrderService {

    /**
     * 创建订单
     * @title createOrder
     * @author hyc
     * @date 2022/2/16 15:18
     * @param order o
     * @return void
     */
    void createOrder(Order order);
}
