package com.hyc.cloud.service;

import com.hyc.cloud.entities.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * 这里是类说明.
 *
 * @className: AccountService
 * @author: hyc
 * @date: 2022-02-16 15:24
 */
@FeignClient(value = "seata-account-service")
public interface AccountService {

    /**
     * 扣减账户余额
     * @title decrease
     * @author hyc
     * @date 2022/2/16 15:25
     * @param userId    用户id
     * @param money 金额
     * @return com.hyc.cloud.entities.CommonResult
     */
    //@RequestMapping(value = "/account/decrease", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    @PostMapping("/account/decrease")
    CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);
}