package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 这里是类说明.
 *
 * @className: SeataOrderMainApp2001
 * @author: hyc
 * @date: 2022-02-16 14:53
 */
@EnableFeignClients
@EnableDiscoveryClient
// 取消数据源的自动创建
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SeataOrderMainApp2001 {

    public static void main(String[] args) {

        SpringApplication.run(SeataOrderMainApp2001.class, args);
    }
}