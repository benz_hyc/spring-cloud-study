package com.hyc.cloud.dao;

import com.hyc.cloud.entities.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 这里是类说明.
 *
 * @className: OrderDao
 * @author: hyc
 * @date: 2022-02-16 14:56
 */
@Mapper
public interface OrderDao {

    /**
     * 创建订单
     * @title createOrder
     * @author hyc
     * @date 2022/2/16 15:01
     * @param order
     * @return void
     */
    int createOrder(Order order);

    /**
     * 修改订单状态
     * @title updateOrderStatus
     * @author hyc
     * @date 2022/2/16 15:02
     * @param id    订单id
     * @param userId    用户id
     * @param status    订单状态：0：创建中；1：已完结
     * @return int
     */
    int updateOrderStatus(@Param("id") Long id, @Param("userId") Long userId, @Param("status") Integer status);
}
