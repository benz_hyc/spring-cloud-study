package com.hyc.cloud.controller;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.entities.Payment;
import com.hyc.cloud.service.PaymentFeignService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 这里是类说明.
 *
 * @className: OrderFeignController
 * @author: hyc
 * @date: 2022-01-14 15:50
 */
@RestController
public class OrderFeignController {

    @Resource
    private PaymentFeignService paymentFeignService;

    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        return paymentFeignService.getPaymentById(id);
    }

    /**
     * OpenFeign超时控制测试
     * 默认Feign客户端只等待一秒钟，但是服务端处理需要超过1秒钟，导致Feign客户端不想等待了，直接返回报错。
     * 为了避免这样的情况，有时候我们需要设置Feign客户端的超时控制。
     * @title paymentFeignTimeout
     * @author hyc
     * @date 2022/1/14 16:31
     * @param
     * @return java.lang.String
     */
    @GetMapping("/consumer/payment/feign/timeout")
    public String paymentFeignTimeout() {

        return paymentFeignService.paymentFeignTimeout();
    }
}