package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 这里是类说明.
 *
 * @className: OrderFeignMain80
 * @author: hyc
 * @date: 2022-01-14 15:38
 */
// 开启Feign的支持
@EnableFeignClients
@SpringBootApplication
public class OrderFeignMain80 {

    public static void main(String[] args) {

        SpringApplication.run(OrderFeignMain80.class, args);
    }
}