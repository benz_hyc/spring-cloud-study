package com.hyc.cloud.controller;

import com.hyc.cloud.entities.CommonResult;
import com.hyc.cloud.entities.Payment;
import com.hyc.cloud.mylb.MyLoadBalancer;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 这里是类说明.
 *
 * @className: OrderController
 * @author: hyc
 * @date: 2022-01-12 10:36
 */
@RestController
public class OrderController {

//    private static final String PAYMENT_SERVER_URL = "http://localhost:8001";

    // 通过在eureka上注册过的微服务名称调用
    private static final String PAYMENT_SERVER_URL = "http://CLOUD-PAYMENT-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/consumer/payment/create") //客户端用浏览器是get请求，但是底层实质发送post调用服务端8001
    public CommonResult create(Payment payment) {
        return restTemplate.postForObject(PAYMENT_SERVER_URL + "/payment/create", payment, CommonResult.class);
    }


    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult getPayment(@PathVariable Long id) {
        return restTemplate.getForObject(PAYMENT_SERVER_URL + "/payment/get/" + id, CommonResult.class, id);
    }

    /**
     * 获取服务列表和服务实例
     * @title discovery
     * @author hyc
     * @date 2022/1/13 10:48
     * @param
     * @return com.hyc.cloud.entities.CommonResult
     */
    @GetMapping("/consumer/discovery")
    public CommonResult discovery() {
        // 通过discoveryClient获取服务列表
        List<String> services = discoveryClient.getServices();

        // 通过discoveryClient 获取某个服务的实例
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");

        Map<String, Object> discovery = new HashMap<>();
        discovery.put("services", services);
        discovery.put("instances", instances);

        return new CommonResult(discovery);
    }

    @GetMapping("/consumer/payment/getForEntity/{id}")
    public CommonResult<Payment> getPaymentInfo(@PathVariable("id") Long id) {
        ResponseEntity<CommonResult> forEntity = restTemplate.getForEntity(PAYMENT_SERVER_URL + "/payment/get/" + id, CommonResult.class, id);
        if (forEntity.getStatusCode().is2xxSuccessful()) {
            return forEntity.getBody();
        } else {
            return new CommonResult("操作失败");
        }
    }

    @Resource
    private MyLoadBalancer myLoadBalancer;

    /**
     * 手写轮询负载均衡算法应用
     * @title paymentLb
     * @author hyc
     * @date 2022/1/14 15:05
     * @param
     * @return java.lang.String
     */
    @GetMapping("/consumer/payment/lb")
    public String paymentLb() {
        // 铜鼓服务名称获取服务下的实例列表
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        if (instances == null || instances.size() < 1) {
            return null;
        }
        // 获取服务实例
        ServiceInstance serviceInstance = myLoadBalancer.getServiceInstance(instances);
        URI uri = serviceInstance.getUri();
        return restTemplate.getForObject(uri + "/payment/lb", String.class);
    }

    /**
     * 链路追踪zipkin测试
     * @title zipkin
     * @author hyc
     * @date 2022年02月09日 23:50
     * @param
     * @return java.lang.String
     */
    @GetMapping("/consumer/zipkin")
    public String zipkin() {
        return restTemplate.getForObject(PAYMENT_SERVER_URL + "/payment/zipkin", String.class);
    }
}