package com.hyc.cloud.mylb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * 手写轮询负载均衡算法.
 *
 * @className: MyLoadBalancer
 * @author: hyc
 * @date: 2022-01-14 14:38
 */
public interface MyLoadBalancer {

    /**
     * 从服务实例列表中获取一个示例
     * @title getServiceInstance
     * @author hyc
     * @date 2022/1/14 14:45
     * @param serviceInstances  s
     * @return org.springframework.cloud.client.ServiceInstance
     */
    ServiceInstance getServiceInstance(List<ServiceInstance> serviceInstances);
}