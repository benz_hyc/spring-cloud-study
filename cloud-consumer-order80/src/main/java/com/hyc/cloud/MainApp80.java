package com.hyc.cloud;

import com.hyc.myrule.CustomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * 这里是类说明.
 *
 * @className: MainApp80
 * @author: hyc
 * @date: 2022-01-12 10:31
 */
// 开启Ribbon的支持
@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration= CustomRule.class)
// 开启eureka的支持
@EnableDiscoveryClient
@SpringBootApplication
public class MainApp80 {
    
    public static void main(String[] args) {
        
        SpringApplication.run(MainApp80.class, args);
    }
}