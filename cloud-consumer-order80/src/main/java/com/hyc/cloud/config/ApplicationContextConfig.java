package com.hyc.cloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 这里是类说明.
 *
 * @className: ApplicationContextConfig
 * @author: hyc
 * @date: 2022-01-12 10:35
 */
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced// 通过在eureka上注册过的微服务名称调用 手写负载均衡算法使用时将此注解注释
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}