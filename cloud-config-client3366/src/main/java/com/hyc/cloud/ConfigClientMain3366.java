package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: ConfigClientMain3366
 * @author: hyc
 * @date: 2022-01-23 23:18
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigClientMain3366 {

    public static void main(String[] args) {

        SpringApplication.run(ConfigClientMain3366.class, args);
    }
}