package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 访问规则 http://myzuul.com:9527/cloud-provider-hystrix-payment/payment/circuit/1
 *        | 网关服务地址          | |   要路由的服务地址            | |   具体的请求路径   |
 *
 * @className: Zuul9527App
 * @author: hyc
 * @date: 2022-01-17 19:22
 */
// 开启zuul的支持
@EnableZuulProxy
@SpringBootApplication
public class Zuul9527App {
    
    public static void main(String[] args) {
        
        SpringApplication.run(Zuul9527App.class, args);
    }
}