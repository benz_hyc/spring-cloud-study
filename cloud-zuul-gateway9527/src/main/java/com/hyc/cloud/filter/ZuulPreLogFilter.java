package com.hyc.cloud.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 这里是类说明.
 *
 * @className: ZuulPreLogFilter
 * @author: hyc
 * @date: 2022-01-17 22:35
 */
@Component
@Slf4j
public class ZuulPreLogFilter extends ZuulFilter {

    @Override
    public String filterType() {
        // pre：在请求被路由到目标服务前执行，比如权限校验、打印日志等功能；
        // routing：在请求被路由到目标服务时执行
        // post：在请求被路由到目标服务后执行，比如给目标服务的响应添加头信息，收集统计数据等功能；
        // error：请求在其他阶段发生错误时执行。
        return "pre";
    }

    @Override
    public int filterOrder() {
        // 过滤顺序 数字越小越先执行
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        // 是否开启过滤器 true-是 false-否
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        String host = request.getRemoteHost();
        String method = request.getMethod();
        String uri = request.getRequestURI();
        //log.info("=====> Remote host:{},method:{},uri:{}", host, method, uri);
        System.out.println("********" + new Date().getTime());
        return null;
    }
}