package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: PaymentMain8004
 * @author: hyc
 * @date: 2022-01-13 15:07
 */
@EnableDiscoveryClient
@SpringBootApplication
public class PaymentMain8004 {
    
    public static void main(String[] args) {
        
        SpringApplication.run(PaymentMain8004.class, args);
    }
}