package com.hyc.cloud.controller;

import com.hyc.cloud.service.IMessageProvider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 这里是类说明.
 *
 * @className: SendMessageController
 * @author: hyc
 * @date: 2022-01-25 14:00
 */
@RestController
public class SendMessageController {

    @Resource
    private IMessageProvider messageProvider;

    /**
     * 发送消息
     * @title sendMessage
     * @author hyc
     * @date 2022/1/25 14:01
     * @param
     * @return java.lang.String
     */
    @GetMapping(value = "/sendMessage")
    public String sendMessage() {
        return messageProvider.send();
    }
}