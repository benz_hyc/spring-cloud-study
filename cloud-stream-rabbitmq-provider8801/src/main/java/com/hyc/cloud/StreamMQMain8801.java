package com.hyc.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 这里是类说明.
 *
 * @className: StreamMQMain8801
 * @author: hyc
 * @date: 2022-01-25 13:54
 */
@EnableDiscoveryClient
@SpringBootApplication
public class StreamMQMain8801 {
    
    public static void main(String[] args) {
        
        SpringApplication.run(StreamMQMain8801.class, args);
    }
}