package com.hyc.cloud.service;

/**
 * 这里是类说明.
 *
 * @className: IMessageProvider
 * @author: hyc
 * @date: 2022-01-25 13:55
 */
public interface IMessageProvider {

    /**
     * 发送消息
     * @title send
     * @author hyc
     * @date 2022/1/25 13:55
     * @param
     * @return java.lang.String
     */
    String send();
}
