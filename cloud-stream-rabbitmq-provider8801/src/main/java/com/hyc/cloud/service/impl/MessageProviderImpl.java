package com.hyc.cloud.service.impl;

import com.hyc.cloud.service.IMessageProvider;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 这里是类说明.
 *
 * @className: MessageProviderImpl
 * @author: hyc
 * @date: 2022-01-25 13:56
 */
// 可以理解为是一个消息的发送管道的定义
@EnableBinding(Source.class)
@Service
public class MessageProviderImpl implements IMessageProvider {

    @Resource
    private MessageChannel output;

    @Override
    public String send() {
        String serial = UUID.randomUUID().toString();
        this.output.send(MessageBuilder.withPayload(serial).build()); // 创建并发送消息
        System.out.println("***serial: "+serial);

        return serial;

    }
}